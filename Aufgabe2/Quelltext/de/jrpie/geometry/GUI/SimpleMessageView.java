package de.jrpie.geometry.GUI;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;


public class SimpleMessageView extends View {
	String message;
	Dimension d;
	public SimpleMessageView(String s) {
		super();
		message = s;
	}

	@Override
	public void draw(Graphics g, BufferedImage b) {
		if(g instanceof Graphics2D){
			((Graphics2D)g).setRenderingHint(
			        RenderingHints.KEY_TEXT_ANTIALIASING,
			        RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		}
		//g.drawString(message,(int) ((d.width - g.getFontMetrics().getStringBounds(message, g).getWidth())/2),d.height / 2 + 5 );

	}


	@Override
	protected void setSize(Dimension d) {
		this.d = d;
		
	}

}
