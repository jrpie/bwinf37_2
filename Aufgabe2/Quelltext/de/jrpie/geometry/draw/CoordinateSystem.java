package de.jrpie.geometry.draw;

import java.util.ArrayList;
import java.util.List;

import de.jrpie.geometry.Line;
import de.jrpie.geometry.Point;
import de.jrpie.geometry.Polygon;


public class CoordinateSystem {
	List<Point> points;
	List<Line> lines;
	public CoordinateSystem(){
		points = new ArrayList<>();
		lines = new ArrayList<>();
	}
	public List<Point> getPoints() {
		return points;
	}
	public void setPoints(List<Point> points) {
		this.points = points;
	}
	public List<Line> getLines() {
		return lines;
	}
	public void setLines(List<Line> lines) {
		this.lines = lines;
	}
	public void addLine(Line l){
		lines.add(l);
	}
	public void addPoint(Point p){
		points.add(p);
	}
	public void clear(){
		points.clear();
		lines.clear();
	}
	public void add(Object o){
		if(o instanceof Point){
			points.add((Point)o);
		}else
		if(o instanceof Line){
			lines.add((Line)o);
		}else
		if(o instanceof Polygon) {
			Polygon p = (Polygon) o;
			for(Line edge : p.getSides()) {
				add(edge);
			}
		}
	}
	public void addEdge(Point p){
		for(Point tp : points){
			int i = 0;
			for(int d = 0; d < p.getDimension(); d++){
				if(tp.getPosition().get(d) == p.getPosition().get(d)){
					i++;
				}
			}
			if(i >= 2){
				lines.add(new Line(p, tp));
			}
		}
		points.add(p);
	}
	public void remove(Object o) {
		if(o instanceof Point){
			points.remove((Point)o);
		}else
		if(o instanceof Line){
			lines.remove((Line)o);
		}else
		if(o instanceof Polygon) {
			Polygon p = (Polygon) o;
			for(Line edge : p.getSides()) {
				remove(edge);
			}
		}
	}
	public void addAll(List<?> l) {
		for(Object o : l) {
			add(o);
		}
	}

}
