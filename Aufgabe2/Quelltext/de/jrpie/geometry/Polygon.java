package de.jrpie.geometry;

import java.util.ArrayList;
import java.util.List;

import de.jrpie.geometry.exception.NotAVertexException;

public class Polygon {
	private Point[] vertices;
	protected String id = "no id";
	public Polygon(Point...points) {
		
		if(isClockwise(points)) {
			//bring into counterclockwise orientation
			Point[] newPoints = new Point[points.length];
			for(int i = 0; i < points.length; i++) {
				newPoints[i] = points[points.length - i -1];
				
			}
			points = newPoints;
		}
		
		this.vertices = points;
	}
	public double getAngle(Point p) throws NotAVertexException {
		Vector v1 = new Line(p, getNextVertex(p)).getVector();
		Vector v2 = new Line(p, getPreviousVertex(p)).getVector();
		
		
		return v1.getCounterclockwiseAngle(v2);
		
	}
	
	public Line[] getSides() {
		Line[] edges = new Line[vertices.length];
		for(int i = 0; i < vertices.length; i++) {
			Line edge = new Line(vertices[i], vertices[(i+1)%vertices.length]);
			edges[i] = edge;
		}
		
		return edges;
	}
	public Point getNextVertex(Point p) throws NotAVertexException {
		int i = getIndex(p);
		return vertices[(i +1)%vertices.length];
		
	}
	public Point getPreviousVertex(Point p) throws NotAVertexException {
		int i = getIndex(p);
		return vertices[(vertices.length -1 + i) % vertices.length];
	}
	private int getIndex(Point p) throws NotAVertexException {
		for(int i = 0; i < vertices.length; i++) {
			if(vertices[i].equals(p)) {
				return i;
			}
		}
		throw new NotAVertexException(p, this);
	}
	
	public boolean isVertex(Point p) {
		for(int i = 0; i < vertices.length; i++) {
			if(p.equals(vertices[i])) {
				return true;
			}
		}
		return false;
	}
	public List<Point> getVertices(){
		List<Point> vertices = new ArrayList<Point>();
		for(Point p : this.vertices) {
			vertices.add(p);
		}
		return vertices;
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Polygon:");
		for(Point p : getVertices()) {
			sb.append(p);
		}
		return sb.toString();
	}
	private static boolean isClockwise(Point... points) {
		//TODO only works in 2D
		//https://stackoverflow.com/questions/1165647/how-to-determine-if-a-list-of-polygon-points-are-in-clockwise-order
		int a = 0;
		for(int i = 0; i < points.length; i++) {
			Vector v1 = points[i].getPosition();
			Vector v2 = points[(i+1) % points.length].getPosition();
			a += (v2.get(0) - v1.get(0)) * (v2.get(1) + v1.get(1));
		}
		return a > 0;
	}
	public String getID() {
		return id;
	}
	public void setID(String id) {
		this.id = id;
	}
}
