package de.jrpie.geometry;

import de.jrpie.geometry.exception.DimensionException;

public class Matrix {
	double[][] matrix; // rows > columns
	public Matrix(double[][] matrix){
		this.matrix = matrix;
	}
	public Matrix(int r, int c) {
		matrix = new double[r][c];
	}
	/**
	   /XXXX\
	 * |####|
	 * \####/
	 * @return
	 */
	public int getRows(){
		return matrix.length;
	}
	/**
	   /X###\
	 * |X###|
	 * \X###/
	 * @return
	 */
	public int getColumns(){
		return matrix[0].length;
	}
	
	public Matrix add(Matrix m) throws DimensionException{
		if(!(m.getDimension().equals(this.getDimension()))){
			throw new DimensionException("Matrix "+this.toString()+" can't be added to Matrix "+m.toString());
		}
		double[][] newMatrix = new double[matrix.length][matrix[0].length];
		for(int x = 0; x < matrix.length; x ++) {
			for(int y = 0; y < matrix[0].length; y++) {
				newMatrix[x][y] = matrix[x][y] + m.get(x, y);
			}
		}
		return new Matrix(newMatrix);
	}
	public MatrixDimension getDimension(){
		return new MatrixDimension(getRows(), getColumns());
	}
	public Vector getVector(int position){
		return new Vector(matrix[position]);
	}
	public double get(int r, int c){
		return matrix[r][c];
	}
	@Override
	public String toString() {
		String s = "Matrix:\n";
		for(int x = 0; x < matrix.length; x++){
			s += "|";
			for(int y = 0; y < matrix[0].length; y++){
				s+=(int)matrix[x][y]+" ";
			}
			s+="|\n";
		}
		s+="Rows: "+getRows()+"\nColumns "+getColumns();
		return s;
	}
	public void set(int r, int c, double v){
		matrix[r][c]= v;
	}
	public Vector multiply(Vector v){
		double[] nVector = new double[this.getColumns()];
		for(int c = 0; c < this.getColumns(); c++){
			for(int r = 0; r < this.getRows(); r++){
				nVector[c]+= v.get(r)*this.get(r,c);
			}
		}
		return new Vector(nVector);
	}
	public Matrix setRow(int r, Vector v) {
		for(int i = 0; i < v.getDimension() && i < getColumns();  i++) {
			matrix[r][i] = v.get(i);
		}
		return this;
	}
	public Matrix setColumn(int c, Vector v) {
		for(int i = 0; i < v.getDimension() && i < getRows();  i++) {
			matrix[i][c] = v.get(i);
		}
		return this;
	}
	public Matrix removeColumn(int column) {
		double[][] newMatrix = new double[getRows()][getColumns()-1];
		for(int r = 0; r < getRows(); r++) {
			for(int c = 0; c < column; c++) {
				newMatrix[r][c] = matrix[r][c];
			}
			for(int c = column +1; c < getColumns(); c++) {
				newMatrix[r][c-1] = matrix[r][c];
			}
		}
		matrix = newMatrix;
		return this;
	}
	public Matrix removeRow(int row) {
		double[][] newMatrix = new double[getRows()-1][getColumns()];
		for(int c = 0; c < getColumns(); c++) {
			for(int r = 0; r < row; r++) {
				newMatrix[r][c] = matrix[r][c];
			}
			for(int r = row +1; r < getRows(); r++) {
				newMatrix[r-1][c] = matrix[r][c];
			}
		}
		matrix = newMatrix;
		return this;
	}
	public Matrix clone(){
		double[][] clone = new double[getRows()][getColumns()];
		for(int x = 0; x < getRows(); x++){
			for(int y = 0; y < getColumns(); y++){
				clone[x][y] = matrix[x][y];
			}
		}
		return new Matrix(clone);
	}
	public Matrix scale(double scale){
		for(int x = 0; x < getRows(); x++){
			for(int y = 0; y < getColumns(); y++){
				matrix[x][y] = matrix[x][y]*scale;
			}
		}
		return this;
	}
	public double getDeterminant() throws DimensionException {
		if(getRows() != getColumns()) {
			throw new DimensionException("No determinant");
		}
		if(getRows() == 1) {
			return matrix[0][0];
		}else if(getRows() == 2) {
			//in order to increase performance for the 2 x 2 case:
			return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
		}else {
			//recursive computation of determinant:
			double determinant = 0.0;
			int factor = -1;
			for(int i = 0; i < getRows(); i++) {
				factor *= -1;
				Matrix m = this.clone().removeColumn(i).removeRow(0);
				determinant += factor * this.get(0, i)* m.getDeterminant();
			}
			return determinant;
		}
	}
}
