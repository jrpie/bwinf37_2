package de.jrpie.geometry;

import java.awt.Color;

public class Line {
	Point start;
	Point end;
	Color color;
	public Line(Point a, Point b){
		this.start = a;
		this.end = b;
	}
	public Line(Point a, Point b, Color c){
		this.start = a;
		this.end = b;
		this.color = c;
	}
	public Point getStart() {
		return start;
	}
	public Point getCenter() {
		return new Point(start.getPosition().add(getVector().scale(0.5)));
	}
	public void setStart(Point start) {
		this.start = start;
	}
	public Point getEnd() {
		return end;
	}
	public void setEnd(Point end) {
		this.end = end;
	}
	public Color getColor() {
		if(color == null){
			return Color.BLACK;
		}
		return color;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	public Vector getVector() {
		return end.getPosition().add(start.getPosition().scale(-1));
	}
	public String toString() {
		return "Line: "+start.toString() + " - " + end.toString();
	}
	public double getLength() {
		return getVector().getAbsolute();
	}
	@Override
	public boolean equals(Object obj) {
		if(! (obj instanceof Line)) {
			return false;
		}
		
		Line l = (Line) obj;
		return l.getStart().equals(this.getStart()) && l.getVector().equals(this.getVector());
	}
	
	
}
