package de.jrpie.geometry;

public class MatrixDimension {
	int rows;
	int columns;
	
	public MatrixDimension(int rows, int columns){
		this.rows = rows;
		this.columns = columns;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof MatrixDimension){
			MatrixDimension o = (MatrixDimension)obj;
			if(this.rows == o.rows && this.columns == o.columns){
				return true;
			}
		}
		return false;
	}
}
