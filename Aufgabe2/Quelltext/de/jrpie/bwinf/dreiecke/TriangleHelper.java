package de.jrpie.bwinf.dreiecke;

import java.util.List;

import de.jrpie.geometry.Line;
import de.jrpie.geometry.Matrix;
import de.jrpie.geometry.Vector;

public class TriangleHelper {
	public static boolean doIntersect(Triangle a, Triangle b) {
		Line[] sidesA = a.getSides();
		Line[] sidesB = b.getSides();
		
		for(Line la : sidesA) {
			for(Line lb : sidesB) {
				if(doIntersect(la, lb)) {
					return true;
				}
			}
		}
		return false;
	}
	public static boolean doIntersect(Triangle a, List<Triangle> b) {
		
		for(Triangle t : b) {
			if(doIntersect(a, t)) {
				return true;
			}
		}
		return false;
	}
	public static boolean doIntersect(Line l1, Line l2) {
		if (l1.equals(l2)) {
			return false;
		}
		
		if (l1.getStart().roughlyEquals(l2.getStart()) || l1.getEnd().roughlyEquals(l2.getStart())
				|| l1.getStart().roughlyEquals(l2.getEnd()) || l1.getEnd().roughlyEquals(l2.getEnd())) {
			return false;
		}
		double angle = l2.getVector().getAngle(l1.getVector());
		angle = Math.abs(angle);
		
		if( angle < 0.1 || (Math.PI < angle && angle < Math.PI - 0.001)){  
			//lines are parallel
			return false;
		}

		// a * (x,y) = e
		Matrix a = new Matrix(new double[][] { { 0, 0 }, { 0, 0 } });
		a.setColumn(0, l1.getVector());
		a.setColumn(1, l2.getVector().scale(-1));
		Vector e = l1.getStart().getPosition().scale(-1).add(l2.getStart().getPosition().scale(1));
		Matrix mx = a.clone();
		Matrix my = a.clone();
		mx.setColumn(0, e);
		my.setColumn(1, e);
		double determinant = a.getDeterminant();
		
		if(determinant == 0) {
			return false;
		}
		
		double detX = mx.getDeterminant();
		double detY = my.getDeterminant();

		double x = detX / determinant;
		double y = detY / determinant;

		if (x > 0.999 || x < 0.001) {
			return false;
		}
		if (y > 0.999 || y < 0.001) {
			return false;
		}
		return true;
	}
}
