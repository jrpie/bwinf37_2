package de.jrpie.bwinf.dreiecke;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import de.jrpie.geometry.Point;
import de.jrpie.geometry.GUI.PlotterFrame;
import de.jrpie.geometry.GUI.View2D;
import de.jrpie.geometry.draw.CoordinateSystem;
import de.jrpie.geometry.draw.Renderer;

public class Main {
	static List<Triangle> triangles = new ArrayList<Triangle>();
	public static CoordinateSystem cs;
	public static View2D view;

	private static String file;

	public static boolean animation = false;
	public static boolean verbose = false;

	public static void main(String[] args) {
		PlotterFrame frame = new PlotterFrame();
		view = new View2D();
		cs = view.getCoordinateSystem();
		frame.startView(view);
		view.getRenderer().setOrigin(new Point(400, -300));
		view.getRenderer().setScale(Renderer.xy.clone().scale(0.6));
		parseArgs(args);
		
		try {
			readFile();
		} catch (FileNotFoundException e1) {
			System.out.println("Can't read file: " + e1.getLocalizedMessage());
			System.exit(1);
		}

		frame.setTitle("Trianguläre: " + file);

		List<TrianglePlacer> placers = new ArrayList<TrianglePlacer>();
		TrianglePlacer p0 = new TrianglePlacer(triangles);
		TrianglePlacer p1 = new TrianglePlacer(triangles);

		p0.weight = 0;
		placers.add(p0);
		placers.add(p1);

		
		TrianglePlacement bestPlacement = null;
		double bestDistance = Double.POSITIVE_INFINITY;

		int i = 0;

		for (TrianglePlacer placer : placers) {
			TrianglePlacement placement = placer.place();
			double distance = placement.getDistance();
			if(verbose)
				System.out.println("Attempt: " + i + "; Distance: " + distance);
			
			i++;
			if (distance < bestDistance) {
				bestPlacement = placement;
				bestDistance = distance;
			}
			cs.clear();
			view.updateCoordinateSystem();

		}

		List<Triangle> sorted = bestPlacement.getTriangles();
		System.out.println("================================================");
		System.out.println("Best placement:");
		
		for (Triangle t : sorted) {
			System.out.println(t.toString());
		}
		System.out.println("Order (left to right):\n");
		for (Triangle t : sorted) {
			System.out.print(t.getID() + " ");
		}
		System.out.printf("\n\nDistance: %gm",bestPlacement.getDistance());
		try {
			SVGHelper.writeToSVG(sorted, new File("output.svg"));
		} catch (IOException e) {

		}
		updateCS(sorted);

	}

	public static void updateCS(List<Triangle> triangles) {
		cs.clear();
		for (Triangle t : triangles) {
			cs.add(t);
		}
		view.updateCoordinateSystem();
	}

	public static void readFile() throws FileNotFoundException {
		File f = new File(file);
		Scanner sc = new Scanner(f);
		// skip first line
		sc.nextLine();
		triangles.clear();
		int id = 0;
		while (sc.hasNext()) {
			id++;
			String line = sc.nextLine();
			try {
				Triangle t = Triangle.fromString(Integer.valueOf(id).toString(), line);
				triangles.add(t);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		sc.close();
	}

	private static void parseArgs(String[] args) {
		for(String s : args) {
			if(s.equalsIgnoreCase("--verbose")) {
				verbose = true;
			}else if(s.equalsIgnoreCase("--animation")){
				animation = true;
			}else {
				file = s;
			}
		}
		if (file == null) {
			System.out.println("Usage: java -jar triangles.jar <path>");
			System.exit(0);
		}
	}
}
