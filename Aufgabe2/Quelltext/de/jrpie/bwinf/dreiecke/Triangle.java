package de.jrpie.bwinf.dreiecke;

import java.util.List;
import java.util.ArrayList;

import de.jrpie.geometry.Line;
import de.jrpie.geometry.Matrix;
import de.jrpie.geometry.Point;
import de.jrpie.geometry.Polygon;
import de.jrpie.geometry.Vector;

public class Triangle extends Polygon {
	public Triangle(String id, Point a, Point b, Point c) {
		super(a, b, c);
		this.id = id;
	}

	public static Triangle fromString(String id, String s) throws Exception {
		String[] numbers = s.split(" ");
		List<Point> points = new ArrayList<Point>();
		Point tmp = null;
		for (int i = 1; i < numbers.length; i++) {
			String n = numbers[i];
			int x = Integer.valueOf(n);
			if (tmp == null) {
				tmp = new Point(x, 0);
			} else {
				tmp = new Point(tmp.getPosition().get(0), x);
				points.add(tmp);
				tmp = null;
			}
		}

		return new Triangle(id, points.get(0), points.get(1), points.get(2));
	}

	public Point getSmallestAngleVertex() {
		double smallest = Double.POSITIVE_INFINITY;
		Point vertex = null;
		for (Point p : getVertices()) {
			if (getAngle(p) < smallest) {
				smallest = getAngle(p);
				vertex = p;
			}
		}
		if (vertex == null) {
			System.err.print(toString());
		}
		return vertex;
	}
	public double getSmallestAngle() {
		return getAngle(getSmallestAngleVertex());
	}

	public Point getOppositePoint(Line l) {
		Point a = l.getStart();
		Point b = l.getEnd();

		Point c = getNextVertex(b);
		if (c.equals(a)) {
			c = getPreviousVertex(b);
		}
		return c;
	}

	public Line getShortestSide() {
		Line shortest = null;
		for (Line l : getSides()) {
			if (shortest == null || l.getLength() < shortest.getLength()) {
				shortest = l;
			}
		}
		return shortest;
	}
	public Triangle move(Vector v) {
		return new Triangle(id, new Point(getVertices().get(0).getPosition().add(v)),
				new Point(getVertices().get(1).getPosition().add(v)),
				new Point(getVertices().get(2).getPosition().add(v)));
	}
	public Triangle move(Point from, Point to) {
		Vector direction = to.getPosition().add(from.getPosition().scale(-1));
		return move(direction);
	}

	public Triangle rotate(Point center, double angle) {
		Point[] newPoints = new Point[3];
		Matrix rotation = new Matrix(new double[][] { new double[] { Math.cos(angle), -Math.sin(angle) },
				new double[] { Math.sin(angle), Math.cos(angle) } });
		for (int i = 0; i < 3; i++) {
			Point p = getVertices().get(i);
			Vector cp = p.getPosition().add(center.getPosition().scale(-1));
			cp = rotation.multiply(cp);
			Point newPoint = new Point(cp.add(center.getPosition()));
			newPoints[i] = newPoint;
		}
		return new Triangle(id, newPoints[0], newPoints[1], newPoints[2]);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Triangle "+id+":");
		for(Point p : getVertices()) {
			sb.append(p.toString()+";");
		}
		return sb.toString();
	}
}
