package de.jrpie.bwinf.dreiecke;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import de.jrpie.geometry.Line;
import de.jrpie.geometry.Point;
import de.jrpie.geometry.Vector;
import de.jrpie.geometry.exception.NotAVertexException;

public class TrianglePlacer {
	private List<Triangle> triangles;
	
	private List<Triangle> previouslyPlacedVertexTriangles = new ArrayList<Triangle>();
	long weight = 1000000;
	
	public TrianglePlacer(List<Triangle> triangles) {
		this.triangles = triangles;
		for(Triangle t : triangles) {
			if(1000 * t.getShortestSide().getLength() > weight) {
				weight =(int) (1000 * t.getShortestSide().getLength()) + 100000;
			}
		}
	}

	public TrianglePlacement place() {

		TrianglePlacement placement = null;
		int n = 0;
		do {
			
			placement = placeTriangles(n++);

		} while (placement == null);
		
		return placement;
	}

	private TrianglePlacement placeTriangles(int numBaseTriangles) {
		List<Triangle> sortedBySmallestAngle = new ArrayList<Triangle>();
		List<Triangle> bestBaseTriangles = new ArrayList<Triangle>();
		
		sortLists(bestBaseTriangles, sortedBySmallestAngle);
		List<Triangle> placed = new ArrayList<Triangle>();
		TrianglePlacement p = new TrianglePlacement(numBaseTriangles);
		
		double[] angles = new double[numBaseTriangles + 1];
		double[] anglesLeft = new double[numBaseTriangles + 1];
		double[] anglesRight = new double[numBaseTriangles + 1];
		double[] positions = new double[numBaseTriangles + 1];
		positions[0] = 0;
		for (int i = 0; i < angles.length; i++) {
			angles[i] = Math.PI;
		}
		double distance = 0.0;
		
		// place base triangles
		for (int i = 0; i < numBaseTriangles; i++) {

			int x = 0;
			Triangle t = bestBaseTriangles.get(0);
			boolean flip = false;
			double angle = t.getAngle(t.getShortestSide().getStart());
			while (angles[i] < angle) {
				x++;
				if (x / 2 < bestBaseTriangles.size()) {
					if (x % 2 == 0) {
						flip = false;
						t = bestBaseTriangles.get(x / 2);
						angle = t.getAngle(t.getShortestSide().getStart());
					} else {

						flip = true;
						t = bestBaseTriangles.get(x / 2);
						angle = t.getAngle(t.getShortestSide().getEnd());
					}
				} else {
					t = bestBaseTriangles.get(0);
					break;
				}

			}
			bestBaseTriangles.remove(t);
			sortedBySmallestAngle.remove(t);
			t = moveBaseTriangleToPosition(t, positions[i]);
			if (flip) {
				Point c = t.getOppositePoint(t.getShortestSide());
				double centerX = t.getShortestSide().getCenter().getPosition().get(0);
				Point newC = new Point(-1 * (c.getPosition().get(0)) + 2 * centerX, c.getPosition().get(1));
				t = new Triangle(t.getID(), t.getShortestSide().getStart(), t.getShortestSide().getEnd(), newC);
			}
			while (TriangleHelper.doIntersect(t, placed)) {
				positions[i] = positions[i] + 1;
				distance += 1;
				t = moveBaseTriangleToPosition(t, positions[i]);
			}

			distance += t.getShortestSide().getLength();
			positions[i + 1] = distance;
			angles[i] -= t.getAngle(t.getShortestSide().getStart());
			angles[i + 1] -= t.getAngle(t.getShortestSide().getEnd());
			anglesLeft[i + 1] = t.getAngle(t.getShortestSide().getEnd());
			anglesRight[i] = t.getAngle(t.getShortestSide().getStart());

			placed.add(t);
			p.setBaseTriangle(i, t);

		}
		//place vertex triangles
		for (int i = 0; i < angles.length; i++) {
			List<Triangle> possibleVertexTriangles = new ArrayList<Triangle>();
			for (Triangle t : sortedBySmallestAngle) {
				possibleVertexTriangles.add(t);
			}
			vertexTriangles: while (true) {
				Triangle nextVertexTriangle = null;
				for (Triangle t : possibleVertexTriangles) {
					if (t.getSmallestAngle() < angles[i]) {
						nextVertexTriangle = t;
						break;
					}
				}
				if (nextVertexTriangle != null) {

					possibleVertexTriangles.remove(nextVertexTriangle);
					previouslyPlacedVertexTriangles.add(nextVertexTriangle);

					Triangle movedLeft = moveVertexTriangleToPosition(nextVertexTriangle, positions[i], angles[i], anglesLeft[i],
							anglesRight[i], true);
					Triangle movedRight = moveVertexTriangleToPosition(nextVertexTriangle, positions[i], angles[i], anglesLeft[i],
							anglesRight[i], false);
					if (Main.animation) {
						Main.cs.clear();
						Main.cs.addAll(placed);
						Main.cs.add(movedLeft);
						Main.cs.add(movedRight);
						Main.view.updateCoordinateSystem();
						try {
							Thread.sleep(10);
						} catch (InterruptedException e) {}
					}

					if (!TriangleHelper.doIntersect(movedLeft, placed)) {
						sortedBySmallestAngle.remove(nextVertexTriangle);
						anglesLeft[i] += nextVertexTriangle.getSmallestAngle();
						angles[i] -= nextVertexTriangle.getSmallestAngle();
						placed.add(movedLeft);
						p.insertVertexTriangle(i, movedLeft, true);
					} else if (!TriangleHelper.doIntersect(movedRight, placed)) {
						sortedBySmallestAngle.remove(nextVertexTriangle);
						anglesRight[i] += nextVertexTriangle.getSmallestAngle();
						angles[i] -= nextVertexTriangle.getSmallestAngle();
						placed.add(movedRight);
						p.insertVertexTriangle(i, movedRight, false);
					}

				} else {
					break vertexTriangles;
				}
			}
		}
		Line distanceL = new Line(new Point(0, 0), new Point(distance, 0));
		distanceL.setColor(Color.BLUE);
		if (Main.animation) {
			Main.cs.clear();
			Main.cs.addAll(placed);
			Main.cs.add(distanceL);
			Main.view.updateCoordinateSystem();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
			}
		}
		return sortedBySmallestAngle.isEmpty() ? p : null;
	}

	private void sortLists(List<Triangle> bestBaseTriangles, List<Triangle> sortedBySmallestAngle) {
		bestBaseTriangles.clear();
		sortedBySmallestAngle.clear();
		for (Triangle t : triangles) {
			sortedBySmallestAngle.add(t);
			bestBaseTriangles.add(t);
		}
		sortedBySmallestAngle.sort(new Comparator<Triangle>() {

			@Override
			public int compare(Triangle t1, Triangle t2) {
				try {
					return (int) (1000
							* (t1.getAngle(t1.getSmallestAngleVertex()) - t2.getAngle(t2.getSmallestAngleVertex())));
				} catch (NotAVertexException e) {
					return 0;
				}
			}
		});
		bestBaseTriangles.sort(new Comparator<Triangle>() {

			@Override
			public int compare(Triangle t1, Triangle t2) {
				return (int) (1000 * (t1.getShortestSide().getLength() - t2.getShortestSide().getLength())
						+ (previouslyPlacedVertexTriangles.contains(t1) ? weight : 0)
					    - (previouslyPlacedVertexTriangles.contains(t2) ? weight : 0));

			}
		});
	}
	

	private Triangle moveVertexTriangleToPosition(Triangle t, double position, double angle,
			double angleLeft, double angleRight, boolean left) {
		t = t.move(t.getSmallestAngleVertex(), new Point(position, 0));
		Line side = left ? new Line(t.getPreviousVertex(t.getSmallestAngleVertex()), t.getSmallestAngleVertex())
				: new Line(t.getSmallestAngleVertex(), t.getNextVertex(t.getSmallestAngleVertex()));

		double rotation = new Vector(1, 0).getCounterclockwiseAngle(side.getVector());
		t = t.rotate(t.getSmallestAngleVertex(), rotation + (left ? angleLeft : (-angleRight)));
		return t;
	}

	private Triangle moveBaseTriangleToPosition(Triangle t, double position) {
		Line shortest = t.getShortestSide();
		Point a = shortest.getStart();
		Point target = new Point(position, 0);
		t = t.move(a, target);

		shortest = t.getShortestSide();
		a = shortest.getStart();
		double angle = shortest.getVector().getCounterclockwiseAngle(new Vector(1, 0));
		t = t.rotate(a, -angle);
		return t;
	}
}
