package de.jrpie.bwinf.dreiecke;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import de.jrpie.geometry.Line;

public class TrianglePlacement {
	
	private final int numBaseTriangles;
	private final Triangle[] baseTriangles;
	private List<List<Triangle>> vertexTrianglesLeft;
	private List<List<Triangle>> vertexTrianglesRight;
	public TrianglePlacement(int numBaseTriangles) {
		this.numBaseTriangles = numBaseTriangles;
		this.baseTriangles = new Triangle[numBaseTriangles];
		vertexTrianglesLeft = new ArrayList<List<Triangle>>();
		vertexTrianglesRight = new ArrayList<List<Triangle>>();
		for(int i = 0; i <= numBaseTriangles; i++) {
			vertexTrianglesLeft.add(new LinkedList<Triangle>());
			vertexTrianglesRight.add(new LinkedList<Triangle>());
		}
	}
	
	public double getDistance() {
		if(numBaseTriangles == 0) {
			return 0;
		}
		double distance =
				new Line(baseTriangles[0].getShortestSide().getStart(),
						baseTriangles[numBaseTriangles-1].getShortestSide().getEnd() ).getLength();
		if(getTrianglesAt(0).isEmpty()) {
			distance -= baseTriangles[0].getShortestSide().getLength();
		}
		if(numBaseTriangles > 1 && getTrianglesAt(numBaseTriangles).isEmpty()) {
			distance -= baseTriangles[numBaseTriangles - 1].getShortestSide().getLength();
		}
		return distance;
	}
	
	public List<Triangle> getTrianglesAt(int position) {
		List<Triangle> trianglesAtPosition = new ArrayList<Triangle>();
		for(Triangle t : vertexTrianglesLeft.get(position)) {
			trianglesAtPosition.add(t);
		}
		for(Triangle t : vertexTrianglesRight.get(position)) {
			trianglesAtPosition.add(t);
		}
		return trianglesAtPosition;
	}
	public List<Triangle> getTriangles(){
		List<Triangle> allTriangles = new LinkedList<Triangle>();
		for(int i = 0; i < numBaseTriangles; i++) {
			allTriangles.addAll(getTrianglesAt(i));
			allTriangles.add(baseTriangles[i]);
		}
		allTriangles.addAll(getTrianglesAt(numBaseTriangles));
		return allTriangles;
	}
	public void setBaseTriangle(int position, Triangle t) {
		baseTriangles[position] = t;
	}
	public Triangle getBaseTriangle(int position) {
		return baseTriangles[position];
	}
	public void insertVertexTriangle(int position, Triangle t, boolean left) {
		if(left) {
			vertexTrianglesLeft.get(position).add(t);		
		}else {
			vertexTrianglesRight.get(position).add(0, t);
		}
		
	}

}
