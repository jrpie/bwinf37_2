package de.jrpie.bwinf.dreiecke;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import de.jrpie.geometry.Point;
import de.jrpie.geometry.Polygon;

public class SVGHelper {
	public static final String svgStart = "<svg version=\"1.1\" viewBox=\"%viewBox%\" xmlns=\"http://www.w3.org/2000/svg\">\n" + 
			"  <g transform=\"scale(1 -1)\">\n" + 
			"    <g transform=\"%translate%\" fill=\"#ffaa00\">\n";
	
	private static final String svgEnd = 
			"    </g>\n" + 
			"  </g>\n" + 
			"</svg>";
	public static void writeToSVG(List<? extends Polygon> polygons, File f) throws IOException {
		String svg = toSVG(polygons);
		
		FileWriter fw = new FileWriter(f);
		fw.write(svg);
		fw.close();
		
	}
	public static String toSVG(List<? extends Polygon> polygons) {
		int minX = 0;
		int maxX = 0;
		int minY = 0;
		int maxY = 0;
		for(Polygon p : polygons) {
			for(Point e : p.getVertices()) {
				int x = (int) e.getPosition().get(0);
				int y = (int) e.getPosition().get(1);
				if(x < minX) {
					minX = x;
				}
				if(x > maxX) {
					maxX = x;
				}
				if(y < minY) {
					minY = y;
				}
				if(y > maxY) {
					maxY = y;
				}
			}
		}
		String svg = SVGHelper.svgStart;
		svg = svg.replace("%viewBox%", 0  + " " + 0 + " " + (maxX -minX) + " " + (maxY - minY));
		svg = svg.replace("%translate%", "translate(0 "+(int)(minY -maxY )+")");
		
		for(Polygon p : polygons) {
			String polygon = "        <polygon id=\""+p.getID()+"\" points=\"";
			
			for(Point v : p.getVertices()) {
				polygon +=String.format("%f %f ",v.getPosition().get(0) - minX,v.getPosition().get(1)-minY);
			}
					
					
			polygon += "\"/>\n";
			
			svg += polygon;
		}
		svg += svgEnd;
		return svg;
	}
}
