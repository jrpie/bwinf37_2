package de.jrpie.geometry;

public class Path {
	Point[] points;
	
	public Path(Point... points) {
		this.points = points;
	}
	
	public Line[] getLines() {
		Line[] lines = new Line[points.length - 1];
		for(int i = 1; i < points.length; i++) {
			lines[i-1] = new Line(points[i-1], points[i]);
		}
		return lines;
	}
	public double getLength() {
		double length = 0;
		for(Line l : getLines()) {
			length +=l.getLength();
		}
		return length;
	}
	public Point getStart() {
		return points[0];
	}
	public Point getEnd() {
		return points[points.length - 1];
	}

	public boolean contains(Point p) {
		for(Point t : points) {
			if(t.equals(p)) {
				return true;
			}
		}
		return false;
	}
	public Path continueTo(Point p) {
		Point[] newPoints = new Point[points.length + 1];
		System.arraycopy(points, 0, newPoints, 0, points.length);
		
		newPoints[points.length] = p;
		return new Path(newPoints);
	}
	@Override
	public String toString() {
		String s = "";
		for(int i = 1; i < points.length; i++) {
			s += points[i-1].toString() +" -> ";
		}
		s+= getEnd().toString();
		return s;
	}
}
