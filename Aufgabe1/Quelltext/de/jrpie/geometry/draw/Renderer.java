package de.jrpie.geometry.draw;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import de.jrpie.geometry.Line;
import de.jrpie.geometry.Matrix;
import de.jrpie.geometry.Point;
import de.jrpie.geometry.Vector;

public class Renderer {
	
	//Matrices for projection
	public static final Matrix cavalier = new Matrix(new double[][] { { 40, 0 }, { -20, 20 }, { 0, -40 } });
	public static final Matrix xy = new Matrix(new double[][] { { 10, 0 }, { 0, -10 }, { 0, 0 } });
	public static final Matrix xz = new Matrix(new double[][] { { 10, 0 }, { 0, 0 }, { 0, -10 } });
	public static final Matrix yz = new Matrix(new double[][] { { 0, 0 }, { -10, 0 }, { 0, -10 } });
	
	
	Matrix projection;
	boolean showLabel;
	boolean showAxis;
	int width = 0;
	int height = 0;
	Point origin = new Point(0,0);
	static final List<Line> axes = new ArrayList<Line>();
	static {
		final Line xA = new Line(Point.getO(3), new Point(10, 0, 0), Color.RED);
		final Line yA = new Line(Point.getO(3), new Point(0, 10, 0), Color.GREEN);
		final Line zA = new Line(Point.getO(3), new Point(0, 0, 10), Color.BLUE);
		axes.add(xA);
		axes.add(yA);
		axes.add(zA);
	}

	public Renderer(Matrix projection, int width, int height) {
		this.projection = projection;
		this.width = width;
		this.height = height;
		this.showLabel = false;
		centerOrigin();
	}

	public Renderer(Matrix projection, int width, int height, boolean label, boolean showAxis) {
		this.projection = projection;
		this.width = width;
		this.height = height;
		this.showLabel = label;
		this.showAxis = showAxis;
		centerOrigin();
	}

	public BufferedImage render(CoordinateSystem cs) {
		BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics g = bi.getGraphics();
		
		//enable AA if available
		if(g instanceof Graphics2D) {
			RenderingHints rh = new RenderingHints(
					RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);
			((Graphics2D) g).addRenderingHints(rh);
		}
		
		//draw white background
		g.setColor(Color.white);
		g.fillRect(0, 0, width, height);
		
		for (Point point : cs.getPoints()) {
			drawPoint(point, g);
		}
		for (Line l : cs.getLines()) {
			drawLine(l, g);
		}

		if (showAxis) {
			for (Line l : axes) {
				drawLine(l, g);
			}
			g.setColor(Color.BLACK);
			g.fillRect(getPositionX(Point.getO(3).getPosition(), origin) - 5,
					getPositionY(Point.getO(3).getPosition(), origin) - 5, 10, 10);
		}
		return bi;
	}

	private void drawPoint(Point p, Graphics g) {
		Vector positionVector = projection.multiply(p.getPosition());
		g.setColor(p.getColor());
		g.drawRect(getPositionX(positionVector, origin) - 2, getPositionY(positionVector, origin) - 2, 4, 4);
		if (showLabel) {
			g.drawString(p.getLabel(), getPositionX(positionVector, origin) + 5, getPositionY(positionVector, origin) + 5);
		}
	}

	private void drawLine(Line l, Graphics g) {
		Vector nP = projection.multiply(l.getStart().getPosition());
		Vector nP2 = projection.multiply(l.getEnd().getPosition());
		g.setColor(l.getColor());
		g.drawLine(getPositionX(nP, origin), getPositionY(nP, origin), getPositionX(nP2, origin),
				getPositionY(nP2, origin));
	}

	private int getPositionX(Vector v, Point origin) {
		return (int) (origin.getPosition().get(0) + v.get(0));
	}

	private int getPositionY(Vector v, Point origin) {
		return (int) (v.get(1) - origin.getPosition().get(1));
	}

	private void centerOrigin() {
		origin = new Point(width / 2, -height / 2);
	}

	public void setOrigin(Point p) {
		origin = p;
	}

	public Matrix getScale() {
		return projection;
	}

	public void setScale(Matrix scale) {
		this.projection = scale;
	}

}
