package de.jrpie.geometry;

import java.awt.Color;

public class Point {
	Vector position;
	Color color;
	String label;
	public Point( double... point){
		this.position = new Vector(point);
		this.label = "";
	}
	public Point(String label, double... point){
		this.position = new Vector(point);
		this.label = label;
	}

	public Point(String label,Color c, double... point){
		this.label = label;
		color = c;
		this.position = new Vector(point);
	}
	public Point(Vector position){
		this.position = position;
	}
	public Point(Color c, Vector position){
		this.color = c;
		this.position = position;
	}
	public int getDimension(){
		return position.getDimension();
	}
	
	public Vector getPosition() {
		return position;
	}
	public void setPosition(Vector position) {
		this.position = position;
	}
	public static final Point getO(int dimension){
		return new Point("O",new double[dimension]);
	}
	public Color getColor() {
		if(color == null){
			return Color.BLACK;
		}
		return color;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	public double getDistance(Point p) {
		return p.getPosition().add(this.getPosition().scale(-1)).getAbsolute();
	}
	public String getFullLabel() {
		String label = (this.label == null ? "":(this.label))+"(";
		for(int x = 0; x < getDimension(); x++){
			label +=  ((int)(getPosition().get(x) * 1000)) / 1000.0;
			if(x+1 < getDimension()){
				label+=" | ";
			}
		}
		label +=")";
		return label;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Point) {
			Point p = (Point) obj;
			return p.getPosition().equals(this.getPosition());
		}
		return false;
	}
	@Override
	public String toString() {
		return getFullLabel();
	}
	
	public boolean roughlyEquals(Object obj) {
		if(obj instanceof Point) {
			Point p = (Point) obj;
			return p.getPosition().roughlyEquals(this.getPosition());
		}
		return false;
	}
	
}
