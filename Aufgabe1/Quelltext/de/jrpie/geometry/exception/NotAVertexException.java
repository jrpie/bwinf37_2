package de.jrpie.geometry.exception;

import de.jrpie.geometry.Point;
import de.jrpie.geometry.Polygon;

public class NotAVertexException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Point a;
	Polygon b;
	public NotAVertexException(Point a, Polygon b) {
		this.a = a;
		this.b = b;
	}
	
	
	public String getCustomMessage() {
		return "Point "+
				(a == null ? "null" : a.toString())+
				"is no vertex of polygon "+
				(b == null ? "null" : b.toString());
	}
	@Override
	public String getLocalizedMessage() {
		return this.getCustomMessage() + "\n" + super.getLocalizedMessage();
	}

}
