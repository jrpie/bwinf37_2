package de.jrpie.geometry.exception;

public class DimensionException extends RuntimeException {
	String exception;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public DimensionException(String exception){
		this.exception = exception;
	}
	@Override
	public String getMessage() {
		return exception;
	}
}
