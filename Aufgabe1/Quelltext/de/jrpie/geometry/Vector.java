package de.jrpie.geometry;

import de.jrpie.geometry.exception.DimensionException;

public class Vector {
	double[] vector;

	public Vector(double... vector) {
		this.vector = vector;
	}

	public int getDimension() {
		return vector.length;
	}

	@Override
	public String toString() {
		String s = "Vector:\n";
		for (int x = 0; x < vector.length; x++) {
			s += "|";
			s += vector[x];
			s += "|\n";
		}
		return s;
	}

	public Vector scale(double scalar) {
		double[] newVector = new double[vector.length];
		for (int i = 0; i < getDimension(); i++) {
			newVector[i] = vector[i] * scalar;
		}
		return new Vector(newVector);
	}

	public double get(int pos) {
		if (pos >= getDimension()) {
			return 0;
		}
		return vector[pos];
	}

	public Vector add(Vector v) {
		int newDimension = Math.max(getDimension(), v.getDimension());
		double[] newVector = new double[newDimension];
		for (int i = 0; i < newDimension; i++) {
			newVector[i] = get(i) + v.get(i);
		}
		return new Vector(newVector);
	}

	public double getAbsolute() {
		double sum = 0;
		for (int x = 0; x < vector.length; x++) {
			sum += vector[x] * vector[x];
		}

		return Math.sqrt(sum);
	}

	public double dotProduct(Vector v) {
		double dotProduct = 0;
		for (int d = 0; d < Math.max(this.getDimension(), v.getDimension()); d++) {
			dotProduct += this.get(d) * v.get(d);
		}
		return dotProduct;
	}

	public double getAngle(Vector v) {
		return Math.acos(Math.abs(dotProduct(v) / (getAbsolute() * v.getAbsolute())));
	}

	public double getCounterclockwiseAngle(Vector v) {
		// only works for 2D vectors
		// https://stackoverflow.com/a/16544330
		double dot = this.dotProduct(v);
		double det = new Matrix(2,2).setColumn(0, this).setColumn(1, v).getDeterminant();
		double a = Math.atan2(det, dot);
		a += 2 * Math.PI;
		a %= 2 * Math.PI;
		return a;

	}

	public Vector crossProduct(Vector v) {
		if (v.getDimension() > 3 || getDimension() > 3) {
			throw new DimensionException("Can't compute the cross product " + v.toString() + " x " + this.toString());
		}
		double x = this.get(1) * v.get(2) - this.get(2) * v.get(1);
		double y = this.get(2) * v.get(0) - this.get(0) * v.get(2);
		double z = this.get(0) * v.get(1) - this.get(1) * v.get(0);
		return new Vector(x, y, z);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Vector) {
			Vector v = (Vector) obj;
			if (v.getDimension() != this.getDimension()) {
				return false;
			}
			for (int d = 0; d < getDimension(); d++) {
				if (v.get(d) != this.get(d)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	public Vector normalize() {
		double abs = getAbsolute();
		return this.scale(1.0 / abs);
	}

	public boolean roughlyEquals(Object obj) {
		double maxDist = 0.0001;
		if (obj instanceof Vector) {
			Vector v = (Vector) obj;
			if (v.getDimension() != this.getDimension()) {
				return false;
			}
			for (int d = 0; d < getDimension(); d++) {
				if (Math.abs(v.get(d) - this.get(d)) > maxDist) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
}
