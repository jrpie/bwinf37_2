package de.jrpie.geometry.GUI;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public abstract class View {
	public View(){}
	/**
	 * Do all the drawing stuff
	 * @param Graphics g to draw on
	 */
	public abstract void draw(Graphics g, BufferedImage bi);
	/**
	 * View placed in foreground
	 */
	protected void onStart() {}
	/**
	 * View destroyed
	 */
	protected void onDestroyed() {}
	

	protected void setSize(Dimension d) {}
	
}
