package de.jrpie.geometry.GUI;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import de.jrpie.geometry.Line;
import de.jrpie.geometry.Point;
import de.jrpie.geometry.draw.CoordinateSystem;
import de.jrpie.geometry.draw.Renderer;

public class View2D extends View {
	CoordinateSystem coordinateSystem = new CoordinateSystem();
	CoordinateSystem renderingCoordinateSystem = new CoordinateSystem();
	Dimension size;
	Renderer r;
	
	@Override
	public synchronized void draw(Graphics g, BufferedImage bi) {
		g.drawImage(r.render(renderingCoordinateSystem), 0,0, null);
		

	}

	public CoordinateSystem getCoordinateSystem() {
		return coordinateSystem;
	}
	public void setCoordinateSystem(CoordinateSystem cs) {
		this.coordinateSystem = cs;
	}
	public synchronized void updateCoordinateSystem() {
		this.renderingCoordinateSystem = new CoordinateSystem();
		
		for(Point p : coordinateSystem.getPoints()) {
			renderingCoordinateSystem.add(p);
		}
		for(Line l : coordinateSystem.getLines()) {
			renderingCoordinateSystem.add(l);
		}
	}
	public Renderer getRenderer() {
		return r;
	}
	@Override
	protected void setSize(Dimension d) {
		this.size = d;
		r = new Renderer(Renderer.xy.scale(0.06), d.width, d.height,false, false);
		r.setOrigin(new Point(0, 50-d.height));
		
	}
	

}
