package de.jrpie.geometry.GUI;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class PlotterFrame extends JFrame {
	private static final boolean saveImages = false;
	private int counter = 0;
	String file = "/tmp/output/";

	
	private static final long serialVersionUID = 1L;
	private Dimension size;
	private View currentView;
	Thread update = new Thread(){
		public void run() {
			while(true){
				repaint();
				try {
					Thread.sleep(16);
				} catch (InterruptedException e) {
				}
			}
			
		};
	};
	private Container pane = new Container(){
		private static final long serialVersionUID = 1L;
		
		public void paint(Graphics g) {
			BufferedImage bi = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);
			if(currentView == null){
				currentView = new SimpleMessageView("ERROR");
			}
			currentView.draw(bi.getGraphics(), bi);
			bi.getGraphics().dispose();
			if(saveImages){
				counter++;
				File outputfile = new File(file+counter+".png");
				if(!outputfile.getParentFile().exists()) {
					outputfile.getParentFile().mkdirs();
				}
				try {
					ImageIO.write(bi, "png", outputfile);
				} catch (IOException e) {
				}
			}
			
			g.drawImage(bi ,0, 0,null);
			
				
		};
		
	};
	public PlotterFrame(){
		super("Plotter");
		size = new Dimension(1000, 600);
		//Full Screen: Toolkit.getDefaultToolkit().getScreenSize();
		if(saveImages){
			number: for(int i = 0; i< 1000; i++){
			File tmp = new File(file+i+"/");
			if(!tmp.exists()){
				tmp.mkdir();
				file+=i+"/";
				break number;
				}
			}
		}
		
		//Full Screen: this.setUndecorated(true);
		this.setSize(size);
		this.setResizable(false);
		this.setContentPane(pane);
		pane.setLayout(null);
		this.setVisible(true);
		
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
			@Override
			public void windowClosed(WindowEvent e) {
				close();
			}
			public void close(){
				System.exit(0);
			}
		});
		update.start();
		
	}
	public View getCurrentView() {
		return currentView;
	}


	public void startView(View v){
		
		View old = getCurrentView();
		if(old != null){
			old.onDestroyed();
		}
		v.setSize(getSize());
		v.onStart();
		currentView = v;
		
		
	}
}
