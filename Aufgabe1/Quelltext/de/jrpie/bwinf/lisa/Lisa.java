package de.jrpie.bwinf.lisa;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

import de.jrpie.geometry.Line;
import de.jrpie.geometry.Matrix;
import de.jrpie.geometry.Path;
import de.jrpie.geometry.Point;
import de.jrpie.geometry.Polygon;
import de.jrpie.geometry.Vector;
import de.jrpie.geometry.GUI.PlotterFrame;
import de.jrpie.geometry.GUI.View2D;
import de.jrpie.geometry.draw.CoordinateSystem;
import de.jrpie.geometry.draw.Renderer;

public class Lisa {
	static Point start;
	static List<Polygon> objects = new ArrayList<Polygon>();
	static List<Line> barriers = new ArrayList<Line>();
	static List<Point> targets = new ArrayList<Point>();
	static List<Point> nodes = new ArrayList<Point>();
	static List<Line> edges = new ArrayList<Line>();

	// GUI
	static CoordinateSystem coordinateSystem;
	static View2D view2d;

	// arguments
	private static boolean headless = false;
	private static long delay = 1000;
	private static String pathToFile = null;
	private static boolean verbose = false;

	public static void main(String[] args) {
		parseArgs(args);

		// init GUI
		view2d = new View2D();
		
		if (!headless) {
			PlotterFrame frame = new PlotterFrame();
			frame.startView(view2d);
			view2d.getRenderer().setScale(Renderer.xy.clone().scale(1.5));
		}

		coordinateSystem = view2d.getCoordinateSystem();
		Point origin = new Point(0, 0);

		coordinateSystem.add(origin);
		Line street = new Line(origin, new Point(0, 1000));
		street.setColor(Color.CYAN);
		coordinateSystem.add(street);

		System.out.println("Reading file...");
		try {
			readFile();
		} catch (FileNotFoundException e) {
			System.out.println("Can't read file: " + e.getLocalizedMessage());
			System.exit(1);
		}

		view2d.updateCoordinateSystem();

		sleep();

		System.out.println("Finding possible paths...");
		findAllEdges();

		sleep();

		System.out.println("Calculating target points...");
		findTargets();

		view2d.updateCoordinateSystem();

		System.out.println("Finding best path...");

		Path p = findBestPath();
		view2d.updateCoordinateSystem();

		if (p != null) {
			for (Line l : p.getLines()) {
				l.setColor(Color.blue);
				coordinateSystem.add(l);
			}
		}
		double vBus = 30 / 3.6;
		double vLisa = 15 / 3.6;
		double time = p.getEnd().getPosition().get(1) / vBus - p.getLength() / vLisa;

		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(23400000 + (int) (time * 1000)); // 07:30 + time

		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");

		System.out.printf(
				"\n\nThe best path is: %s \n" + "Lisa has to run %g m.\n" + "Lisa has to start at %s.\n"
						+ "(%g seconds %s the bus arrives at (0 | 0))",
				p.toString(), p.getLength(), sdf.format(c.getTime()), Math.abs(time), time < 0 ? "before" : "after");

		view2d.updateCoordinateSystem();

		sleep();

		coordinateSystem.clear();

		for (Line l : barriers) {
			l.setColor(Color.black);
			coordinateSystem.add(l);
		}
		for (Line l : p.getLines()) {
			l.setColor(Color.blue);
			coordinateSystem.addLine(l);
		}

		view2d.updateCoordinateSystem();
	}

	static void findAllEdges() {

		for (int a = 0; a < nodes.size() - 1; a++) {
			for (int b = a + 1; b < nodes.size(); b++) {
				Line line = new Line(nodes.get(a), nodes.get(b));
				if (checkEdge(line)) {

					line.setColor(Color.RED);

					edges.add(line);
					coordinateSystem.add(line);
					view2d.updateCoordinateSystem();
				}

			}
		}
		view2d.updateCoordinateSystem();
	}

	static boolean checkEdge(Line edge) {
		Vector v = edge.getVector();
		Point start = edge.getStart();
		Point end = edge.getEnd();
		Polygon p1 = null;
		Polygon p2 = null;
		for (Polygon p : objects) {
			if (p.isVertex(edge.getStart())) {
				p1 = p;
			}
			if (p.isVertex(edge.getEnd())) {
				p2 = p;
			}
		}
		if (p1 != null && p1.equals(p2)) {
			if (p1.getNextVertex(start).equals(end) || p1.getNextVertex(end).equals(start)) {
				return true;
			}

			Line next = new Line(edge.getStart(), p1.getNextVertex(edge.getStart()));
			Line previous = new Line(edge.getStart(), p1.getPreviousVertex(edge.getStart()));

			if (v.getCounterclockwiseAngle(next.getVector()) > v.getCounterclockwiseAngle(previous.getVector())) {
				return false;
			}

		}
		for (Line barrier : barriers) {
			boolean intersect = doIntersect(barrier, edge);

			if (intersect) {
				barrier.setColor(Color.CYAN);
				return false;
			}
		}
		return true;
	}

	static void findTargets() {
		for (Point p : nodes) {
			Point target = getTarget(p);
			Line wayToTarget = new Line(p, target);

			if (checkEdge(wayToTarget)) {
				targets.add(target);
				coordinateSystem.add(target);
				edges.add(wayToTarget);
				coordinateSystem.add(wayToTarget);
			}
		}
	}

	static Point getTarget(Point p) {
		return new Point(0, p.getPosition().get(1) + p.getPosition().get(0) / Math.sqrt(3));
	}

	static Path findBestPath() {
		Path bestPath = null;
		double bestTime = Double.POSITIVE_INFINITY;
		for (Point target : targets) {
			Path p = Pathfinding.getShortestPath(target, start, edges);
			if (verbose) {
				System.out.println("\n\nThe shortest path to target point " + target.toString() + " is:");
				System.out.println(p.toString());
			}
			for (Line l : p.getLines()) {
				l.setColor(Color.CYAN);
				coordinateSystem.add(l);
			}
			view2d.updateCoordinateSystem();

			double time = p.getLength() - (target.getPosition().get(1) / 2.0);

			if (time < bestTime) {
				bestPath = p;
				bestTime = time;
			}
		}
		return bestPath;
	}

	static boolean doIntersect(Line l1, Line l2) {
		if (l1.equals(l2)) {
			return false;
		}
		// Lines, which share a common start or end, are considered to be non
		// intersecting.
		if (l1.getStart().equals(l2.getStart()) || l1.getEnd().equals(l2.getStart())
				|| l1.getStart().equals(l2.getEnd()) || l1.getEnd().equals(l2.getEnd())) {
			return false;
		}

		// a * (x,y) = e
		Matrix a = new Matrix(new double[][] { { 0, 0 }, { 0, 0 } });
		a.setColumn(0, l1.getVector());
		a.setColumn(1, l2.getVector().scale(-1));
		Vector e = l1.getStart().getPosition().scale(-1).add(l2.getStart().getPosition().scale(1));
		Matrix mx = a.clone();
		Matrix my = a.clone();
		mx.setColumn(0, e);
		my.setColumn(1, e);
		double determinant = a.getDeterminant();

		if(determinant == 0) {
			return false;
		}
		double detX = mx.getDeterminant();
		double detY = my.getDeterminant();

		double x = detX / determinant;
		double y = detY / determinant;

		if (x > 1 || x < 0) {
			return false;
		}
		if (y > 1 || y < 0) {
			return false;
		}

		return true;
	}

	static void parseArgs(String[] args) {
		for (String s : args) {
			if (s.equalsIgnoreCase("--headless")) {
				headless = true;
				delay = 0;
			} else if (s.equalsIgnoreCase("--verbose")) {
				verbose = true;
			} else {
				pathToFile = s;
			}
		}
		if (pathToFile == null) {
			System.out.println("Usage: java -jar Lisa.jar <path>");
			System.exit(1);
		}
	}

	static void readFile() throws FileNotFoundException {
		File input = new File(pathToFile);
		Scanner sc = new Scanner(input);
		int numObjects = sc.nextInt();
		for (int o = 0; o < numObjects; o++) {
			int numPoints = sc.nextInt();
			Point[] points = new Point[numPoints];
			for (int p = 0; p < numPoints; p++) {
				int x = sc.nextInt();
				int y = sc.nextInt();
				Point point = new Point(x, y);

				points[p] = point;
				coordinateSystem.add(point);
				nodes.add(point);
			}
			Polygon object = new Polygon(points);
			objects.add(object);
			coordinateSystem.add(object);
		}
		int startX = sc.nextInt();
		int startY = sc.nextInt();
		start = new Point(startX, startY);
		start.setColor(Color.GREEN);
		nodes.add(start);
		coordinateSystem.add(start);
		sc.close();

		for (Polygon p : objects) {
			for (Line l : p.getSides()) {
				barriers.add(l);
			}
		}

	}

	static void sleep() {
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			// I don't care
		}
	}
}
