package de.jrpie.bwinf.lisa;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.jrpie.geometry.Line;
import de.jrpie.geometry.Path;
import de.jrpie.geometry.Point;

public class Pathfinding {
	public static Path getShortestPath(Point target, Point start, List<Line> edges) {
		List<Path> paths = new ArrayList<>();
		List<Path> newPaths = new ArrayList<Path>();
		paths.add(new Path(start));
		Path bestPath = null;
		double shortestLength = Double.MAX_VALUE;
		HashMap<Point, Double> knownNodes = new HashMap<>();
		while (bestPath == null || paths.size() > 0) {
			paths: for (Path path : paths) {
				Path estimate = path.continueTo(target);
				double length = path.getLength();
				if (!knownNodes.containsKey(path.getEnd()) || knownNodes.get(path.getEnd()).doubleValue() > length) {
					knownNodes.put(path.getEnd(), length);
				} else {
					continue paths;
				}

				if (bestPath != null && shortestLength < estimate.getLength()) {
					continue paths;
				} else if (path.getEnd().equals(target)) {
					if (shortestLength > length) {
						bestPath = path;
						shortestLength = path.getLength();
					}
				} else {
					lines: for (Line line : getEdgesAt(path.getEnd(), edges)) {
						if (path.contains(line.getEnd())) {
							continue lines;
						}
						Path newPath = path.continueTo(line.getEnd());
						newPaths.add(newPath);

					}
				}

			}
			paths.clear();
			paths.addAll(newPaths);
			newPaths.clear();

		}

		return bestPath;
	}

	private static List<Line> getEdgesAt(Point node, List<Line> edges) {
		List<Line> tmpEdges = new ArrayList<Line>();
		for (Line edge : edges) {
			if (edge.getStart().equals(node)) {
				edge.setColor(Color.GREEN);
				tmpEdges.add(edge);
			}
			if (edge.getEnd().equals(node)) {
				edge.setColor(Color.green);
				tmpEdges.add(new Line(edge.getEnd(), edge.getStart()));
			}
		}
		return tmpEdges;
	}
}
